package com.example.demo.Service;

import com.example.demo.DAO.UserDao;
import com.example.demo.model.User;

public class UserService {

    private UserDao dao;

    public UserService(UserDao dao) {
        this.dao = dao;
    }

    public boolean checkUserPresence(User user) throws Exception{
        User u = dao.getUserByUsername(user.getUsername());
        return u!=null;
    }
}
